<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
			$this->load->model('User_model');

			//Loading url helper
			$this->load->helper('url');
	}

	public function index()
 	{
		$data['available_field_details'] = $this->User_model->get_all_form_entries();

		$data['input_type'] = $this->User_model->get_type();

		$this->load->view('lead_bolt/Student_list', $data);
 	}

	public function show_user_assign_form()
	{
		 $data['available_fields'] = $this->User_model->get_all_field_entries();
		 $data['available_field_details'] = $this->User_model->get_all_details_entries();
		 $data['available_students'] = $this->User_model->get_all_form_entries();

		$this->load->view('lead_bolt/Assign_user_info_form', $data);
	}

	public function show_fields_add_form(){
			$data['available_field_details'] = $this->User_model->get_all_user_field_entries();

			$data['input_type'] = $this->User_model->get_type();
			$this->load->view('lead_bolt/standard_user_field_form', $data);
	}


	public function save_field_form(){
		$description	=	$this->input->post('description');
		$type	=	$this->input->post('field_form_field_type');
		$status	=	$this->input->post('field_form_field_status');
		$mandatory_status	=	$this->input->post('field_form_field_mandatory_status');

		$data = array(
				'status'=> $status,
				'field_type'=> $type,
				'description'=> $description,
				'mandatory'	=> $mandatory_status
		);

		$this->db->insert('standard_user_fields',$data);

		redirect('/', 'refresh');
	}

	public function save_user_assign_form(){

		$user_id	=	$this->input->post('user_id');
		$field_id	=	$this->input->post('field_id');
		$field_value	=	$this->input->post('field_value');

		if(empty($user_id)){

			$this->db->select_max('user_id');
			$result = $this->db->get('user_details')->row();

			$data = array(
					'user_id' => ($result->user_id)+1,
					'field_id'=> $field_id,
					'value'=> $field_value
			);

			$this->db->insert('user_details',$data);

		}else{

			$data = array(
					'user_id' => $user_id,
					'field_id'=> $field_id,
					'value'=> $field_value
			);

			$this->db->insert('user_details',$data);

		}


		redirect('/', 'refresh');
	}

	public function show_student_form(){
		$uri = $this->uri->total_segments();
		$user_id = $this->uri->segment($uri);

		$data['available_field_details'] = $this->User_model->get_user_field_entries($user_id);

		$data['input_type'] = $this->User_model->get_type();

		$this->load->view('lead_bolt/Student_process_form', $data);
	}

	public function show_student_form_list(){
		$data['available_field_details'] = $this->User_model->get_all_user_field_entries();

		$data['input_type'] = $this->User_model->get_type();

		$this->load->view('lead_bolt/Student_list', $data);
	}

	public function vendor_user_details_form(){

		$user_info = $this->input->post('user_info');



		foreach($user_info['data'] as $user_id => $details){

			foreach($details as $field_id => $value){

				$data = array(
					'value' => $value,
				);

				$this->db->where('user_id', $user_id);
				$this->db->where('field_id', $field_id);
				$this->db->update('user_details', $data);

			}
		}

		redirect('/', 'refresh');
	}


	public function delete_user_details(){

		$uri = $this->uri->total_segments();
		$field_id = $this->uri->segment($uri);

		$this->User_model->delete_user_details($field_id);

		redirect('/', 'refresh');
	}

}
