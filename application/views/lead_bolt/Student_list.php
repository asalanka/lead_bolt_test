<?php
$this->load->helper('form');
$this->load->library('table');
$this->load->view('lead_bolt/common/Header');

    $template = array(
            'table_open'            => '<table border="0" cellpadding="4" cellspacing="0" id="assign_form_tbl">',

            'thead_open'            => '<thead>',
            'thead_close'           => '</thead>',

            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th>',
            'heading_cell_end'      => '</th>',

            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',

            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td>',
            'cell_end'              => '</td>',

            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td>',
            'cell_alt_end'          => '</td>',

            'table_close'           => '</table>'
    );


    $this->table->set_template($template);

    $this->table->set_heading('Id', 'Value','');

    foreach($available_field_details as $row){

      $links  = anchor('student-process-form/'.$row->user_id ,'Edit',"class='btn btn-info'");
      $links .= anchor('student-details/delete/'.$row->user_id , 'Delete',"class='btn btn-danger'");
      $this->table->add_row($row->id, $row->value, $links);
    }


    echo $this->table->generate();


$this->load->view('lead_bolt/common/Footer');
