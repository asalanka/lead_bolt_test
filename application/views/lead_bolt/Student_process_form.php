<?php
$this->load->helper('form');
$this->load->library('table');
$this->load->view('lead_bolt/common/Header');

echo('<div>');
$attributes = array(
        'id'    => 'address_info',
        'class' => 'address_info'
);

/* Form field set start */
echo form_fieldset('Student field Information', $attributes);

/* Form start */
echo form_open('student_info_form/save');


echo ("<div id='userinfo-assign-form' class='form-group'>");

foreach($available_field_details as $row)
{
  $input_type['value'] = $row->value;
  $input_type['name'] = "user_info[data][".$row->user_id."][".$row->field_id."]";
  echo form_label($row->description, 'Field');
  echo form_input($input_type);
}

echo form_submit('field_assign-value-submit', 'Save','class="btn btn-primary"');

echo("</div>");
$string = '';
echo form_close($string);
/* Form ends */

echo form_fieldset_close();
/* Form field set ends */
echo('</div>');

$this->load->view('lead_bolt/common/Footer');
