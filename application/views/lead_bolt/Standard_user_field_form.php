<?php
$this->load->helper('form');
$this->load->view('lead_bolt/common/Header');

$attributes = array(
        'id'    => 'address_info',
        'class' => 'address_info'
);

/* Form field set start */
echo form_fieldset('Add user field Information', $attributes);

/* Form start */
echo form_open('field_form/save');
echo ("<div id='field-type-form' class='form-group'>");

$data = array(
        'name'          => 'description',
        'id'            => 'field_form_desc',
        'value'         => '',
        'maxlength'     => '100',
        'size'          => '50',
        'style'         => 'width:50%',
        'class'         => 'form-control'
);

echo form_label('Description', 'Description');
echo form_input($data);

//

$options = array(
        'Numeric'         => 'Numeric',
        'String'           => 'String',
        'Date'         => 'Date',
);

$field_type_attributes = array(
        'id'    => '',
        'size'          => '',
        'style'         => 'width:50%',
        'class' => 'form-control'
);

echo form_label('Field Type', 'Field Type');
echo form_dropdown('field_form_field_type', $options,'String',$field_type_attributes);

$options = array(
        '1'           => 'Active',
        '2'         => 'Inactive',
);

$field_status_attributes = array(
        'id'    => '',
        'size'          => '',
        'style'         => 'width:50%',
        'class' => 'form-control'
);

echo form_label('Field Status', 'Field Status');
echo form_dropdown('field_form_field_status', $options, '1',$field_status_attributes);

$options = array(
        '1'           => 'Required',
        '2'         => 'Not Required',
);

$field_mandatory_status_attributes = array(
        'id'    => '',
        'size'          => '',
        'style'         => 'width:50%',
        'class' => 'form-control'
);

echo form_label('Mandatory Status', 'Mandatory Status');
echo form_dropdown('field_form_field_mandatory_status', $options, '1',$field_mandatory_status_attributes);

echo form_submit('field_type_submit', 'Save', 'class="btn btn-primary"');

echo("</div>");
$string = '';
echo form_close($string);
/* Form ends */

echo form_fieldset_close();
/* Form field set ends */

$this->load->view('lead_bolt/common/Footer');
