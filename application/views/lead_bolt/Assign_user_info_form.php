<?php
$this->load->helper('form');
$this->load->library('table');
$this->load->view('lead_bolt/common/Header');

$attributes = array(
        'id'    => 'address_info',
        'class' => 'address_info'
);

/* Form field set start */
echo form_fieldset('Add user field Information', $attributes);

/* Form start */
echo form_open('user_info_form/save');


echo ("<div id='userinfo-assign-form'>");

echo form_label('User Id', 'Field');
echo ("<select class='form-control form-group' name='user_id' style='width:50%;'>");
     echo '<option value="0">New Student</option>';
     foreach($available_students as $row)
     {
       echo '<option value="'.$row->user_id.'">'.$row->value.'</option>';
     }

echo ("</select>");

echo form_label('Field', 'Field');
echo ("<select class='form-control form-group' name='field_id' style='width:50%;'>");
     foreach($available_fields as $row)
     {
       echo '<option value="'.$row->field_id.'">'.$row->description.'</option>';
     }

echo ("</select>");



$data = array(
        'name'          => 'field_value',
        'id'            => 'field_form_value',
        'value'         => '',
        'maxlength'     => '100',
        'size'          => '50',
        'style'         => 'width:50%',
        'class'         => 'form-control'
);

echo form_label('Value', 'Value');
echo form_input($data);

echo form_submit('field_assign-value-submit', 'Save','class="btn btn-primary"');

echo("</div>");
$string = '';
echo form_close($string);
/* Form ends */

echo form_fieldset_close();
/* Form field set ends */

$this->load->view('lead_bolt/common/Footer');
