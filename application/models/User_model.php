<?php

class User_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }


        public function get_all_field_entries(){

          $query = $this->db->get('standard_user_fields');

          return $query->result();
        }

        public function get_all_user_field_entries(){
          $this->db->select('*');
          $this->db->from('standard_user_fields');
          $this->db->join('user_details', 'standard_user_fields.field_id = user_details.field_id');

          $query = $this->db->get();

          return $query->result();
        }

        public function get_all_form_entries(){
          $this->db->select('*');
          $this->db->from('user_details');
          //$this->db->join('user_details', 'standard_user_fields.field_id = user_details.field_id');
          $this->db->group_by("user_id");
          $query = $this->db->get();

          return $query->result();
        }

        public function get_user_field_entries($id){
          $this->db->select('*');
          $this->db->from('standard_user_fields');
          $this->db->where('user_details.user_id', $id);
          $this->db->join('user_details', 'standard_user_fields.field_id = user_details.field_id');

          $query = $this->db->get();

          return $query->result();
        }


        public function get_all_details_entries(){

          $query = $this->db->get('user_details');

          return $query->result();
        }


        public function get_type(){

          $return_type =  array(
                'id'            => 'user_info',
                'maxlength'     => '100',
                'size'          => '50',
                'size'          => '50',
                'style'         => 'width:50%',
                'class'         => 'form-control user_info'
            );

          return $return_type;
        }

        public function delete_user_details($id){
          $this->db->where('user_id', $id);
          $this->db->delete('user_details');
        }


}
